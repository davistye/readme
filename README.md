## Quarterly Priorities
This content is meant to communicate how I intend to allocate my time. I review it weekly, but it should largely remain consistent on the time-scales of quarters.

| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Team Development | CDF Reviews, Career Development, Team Engagement, GitLab Values Coaching | 25% |
| Sensing Mechanisms | Internal context gathering, customer interviews, analyst inquiries, competitive review | 15% |
| Cross Section Product Experience | Think Big-Think Small, Walk-throughs, learning goals, direction content review | 15% |
| GTM Engagement | Use Case alignment, Opportunity Review, Sales Support, PMM/TMM Alignment | 10% |
| Product Performance Indicators | Performance Indicator instrumentation, understanding, goal setting and attainment | 10% |
| Product Management Leadership Priorities | Tier Strategy and ROI focus, product portfolio management | 5% |
| External Evangelism | Ops vision, analyst briefings, conference speaking | 5% |
| Core Team Engagement | UX, Development, Quality, Infrastructure shared Performance Indicators and Retrospectives| 10% |
| Personal Growth / Leadership Opportunities | Representing GitLab externally, Representing Product internally | 5% |
| Hiring | Sourcing, interviewing, hiring | 0% |

## Weekly Priorities
This content is meant to communicate my priorities on a weekly basis. They should typically be reflected as items in the `Doing` of [my personal issue board](https://gitlab.com/groups/gitlab-com/-/boards/1353560?assignee_username=kencjohnston). It will be updated weekly. As I'm always striving to learn I'll also add a `Learning Goal` each week. You can see [the history](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=closed&assignee_username[]=kencjohnston&label_name[]=Weekly%20Priorities) of my Weekly Priorities setting issues and the [template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Weekly-Priority-Setting-Kenny.md) I use. 

### Legend
These designations are added to the previous week's priority list when adding the current week's priority.
* **Y** - Completed
* **N** - Not completed

## Current

1. This is an [Example 1] (https://gitlab.com/kencjohnston/README)
1. This is an [Example 1] (https://gitlab.com/kencjohnston/README)
1. This is an [Example 1] (https://gitlab.com/kencjohnston/README)
1. This is an [Example 1] (https://gitlab.com/kencjohnston/README)
1. This is an [Example 1] (https://gitlab.com/kencjohnston/README)
1. This is an [Example 1] (https://gitlab.com/kencjohnston/README)
1. This is an [Example 1] (https://gitlab.com/kencjohnston/README)

